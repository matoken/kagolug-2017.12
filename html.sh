#!/bin/sh

files=`ls -1 *.adoc|egrep -v "about|footer|wifi|kagolug|chromecast"`

if ! test -d html ; then
  mkdir html
fi

for file in $files
do
  html=html/`echo $file | sed 's/\.[^\.]*$//'`.html
#  if ! test -e $html || test $file -nt $html ; then
  if ! test -e $html || test $file -nt $html || test about.adoc -nt $html || test footer.adoc -nt $html ; then
    echo $file to $html
#    asciidoc $file
#    cat `echo $file | sed 's/\.[^\.]*$//'`.html | tr '<img src="image/' '<img src="../image/' > $html
#    rm `echo $file | sed 's/\.[^\.]*$//'`.html
    asciidoc -b html5 -o - $file | sed -e "s/\<img\ src=\"image\//\<img\ src=\"..\/image\//" > $html

  fi
done

