#!/bin/sh

files=`ls -1 *.adoc|egrep -v "about|footer|wifi|kagolug|chromecast"`

if ! test -d cdk ; then
  mkdir cdk
fi

for file in $files
do
  html=cdk/`echo $file | sed 's/\.[^\.]*$//'`.html
  if ! test -e $html || test $file -nt $html || test about.adoc -nt $html || test footer.adoc -nt $html ; then
    echo $file to $html
    cdk $file
    mv `echo $file | sed 's/\.[^\.]*$//'`.html cdk/
  fi
done

